#include <iostream>
using namespace std;

int partition(int vec[], int start, int end) {
	int pivot = vec[end];
	int i = start;
	for (int j = start; j < end; j++) {
		if (vec[j] < pivot) {
			//reorder(vec[i], vec[j]);
			{
				int tmp = vec[i];
				vec[i] = vec[j];
				vec[j] = tmp;
			}
			i = i + 1;
		}
	}
	//reorder(vec[i], vec[end]);
	{
		int tmp = vec[i];
		vec[i] = vec[end];
		vec[end] = tmp;
	}
	return i;
}

void quickSort(int vec[], int start, int end) {
	if (start < end) {
		int p = partition(vec, start, end);
		quickSort(vec, start, p - 1);
		quickSort(vec, p + 1, end);
	}
}


int main() {
	int polje[] = { 10, 7, 8, 9, 1, 5 };
	quickSort(polje, 0, 5);
	for (int i = 0; i < 6; i++) {
		cout << polje[i] << " ";
	}
	cout << endl;

	system("pause");
	return 0;
}