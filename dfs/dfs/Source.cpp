#include <iostream>
#include <stack> 
#include <string>
#include <chrono>
#include <fstream>
#include <vector>
#include <sstream>
#include <queue>
using namespace std;




struct Vozlisce {
	int predhodnik;
	int dolzina;
	int status;
	int indeks;
	int ime;
	vector<int> sosedi;
};stack<Vozlisce> sklad;
queue<Vozlisce> vrsta;
bool prebrano = false;


void preberi(vector<Vozlisce> &x) {
	x.clear();
	ifstream f("graf.txt");
	int stevilo_vozlisc;
	f >> stevilo_vozlisc;
	int stevilo_povezav;
	f >> stevilo_povezav;
	//
	int vrstic = stevilo_vozlisc;
	int stolpcev = stevilo_vozlisc;
	int *C = new int[vrstic*stolpcev];
	for (int i = 0; i < vrstic*stolpcev; i++) {
		C[i] = 0;
	}
	/*for (int i = 0; i < vrstic; i++) {
		delete[]a[i];
	}
	delete[]a;*/

	//ustvari polje V in dodaj vozlisca v V, kjer je ime = indeks;
	for (int i = 0; i < stevilo_vozlisc; i++) {
		Vozlisce tmp;
		tmp.ime = i + 1;
		tmp.indeks = i;
		tmp.dolzina = 0;
		tmp.status = 0;
		tmp.predhodnik = NULL;
		x.push_back(tmp);
	}
	//ustvari matriko sosednosti C(2D polje);
	for (int i = 0; i < stevilo_povezav; i++) {
		int indeksV1;
		int indeksV2;
		int cena;
		f >> indeksV1 >> indeksV2 >> cena;
		indeksV1--;
		indeksV2--;
		//v matriki C oznaci vozlisci v1 in v2 kot soseda
		C[indeksV1*stevilo_vozlisc + indeksV2] = cena;
		C[indeksV2*stevilo_vozlisc + indeksV1] = cena;
		/*for (int j = 0; j < x.size(); j++) {
			if (x[i].ime != indeksV1) {
				Vozlisce tmp;
				tmp.ime = indeksV1;
				tmp.indeks = indeksV1;
				tmp.dolzina = 1;

				x.push_back()
			}
		}*/
		x[indeksV1].sosedi.push_back(indeksV2);
		x[indeksV2].sosedi.push_back(indeksV1);
	}

	/*for (int i = 0; i < stolpcev; i++) {
		for (int j = 0; j < stolpcev; j++) {
		cout << C[i*8+j] << " ";
		}
		cout << endl;
	}*/



	prebrano = true;
	return;
}


void izpis_poti(vector<Vozlisce> x, Vozlisce s, Vozlisce v) {
	if (s.ime == v.ime) {
		cout << "Pot je: " << v.ime;
	}
	else {
		if (v.predhodnik == NULL) {
			cout << "Med " << s.ime << " in " << v.ime << " ni poti!" << endl;
		}
		else {
			//cout <<v.ime<<" pret: "<<v.predhodnik <<" tu sam\n";
			izpis_poti(x, s, x[v.predhodnik - 1]);
			cout << " -> " << v.ime;
		}
	}
}

void iskanje_globina(vector<Vozlisce> &x) {
	auto start = std::chrono::steady_clock::now();
	int s, q;
	cout << "Vnesi zacetno i krajnje vozlisce: ";
	cin >> s >> q;
	int ind = s - 1;
	x[ind].status = 1;
	sklad.push(x[ind]);
	while (!sklad.empty()) {
		Vozlisce tmp = sklad.top();
		sklad.pop();
		ind = tmp.indeks;
		for (int j = 0; j < x[ind].sosedi.size(); j++) {
			//cout <<"OVO SO SOSEDI: "<< x[x[ind].sosedi[j]].ime << "\n";
			if (x[x[ind].sosedi[j]].status == 0) {
				int ind2 = x[x[ind].sosedi[j]].indeks;
				x[ind2].predhodnik = ind + 1;
				x[ind2].status = 1;
				x[ind2].dolzina = x[ind].dolzina + 1;
				//cout << "ve bom pushal: \n"<< x[ind2].ime<<" indeks od kojega provjeravam: "<<ind<<" ind2 "<<ind2<<endl;
				sklad.push(x[x[ind].sosedi[j]]);
			}
		}
		//cout << endl;
		x[ind].status = 2;

	}
	//cout << x[s - 1].ime << " " << x[q - 1].ime << " \n";
	auto end = std::chrono::steady_clock::now();
	cout << "\nCas trajanja: " << chrono::duration_cast<chrono::microseconds>(end - start).count() <<
		" microseconds." << endl;
	izpis_poti(x, x[s - 1], x[q - 1]);
	cout << endl;
	cout << "Dolzina: " << x[q - 1].dolzina << endl;
	/*for (int i = 0; i < x.size(); i++) {
		cout <<x[i].ime<<" "<<x[i].indeks<<" "<< x[i].status<<" "<< x[i].dolzina<<" "<<x[i].predhodnik<<endl;
	}*/


}





int main() {
	vector<Vozlisce> polje;
	preberi(polje);
	iskanje_globina(polje);


	system("pause");
	return 0;
}