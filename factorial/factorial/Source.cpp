#include <iostream>
using namespace std;


#define MAX 500 

int mnozenje(int x, int rez[], int velikost)
{
	int carry = 0; 

	
	for (int i = 0; i < velikost; i++)
	{
		int prod = rez[i] * x + carry;
		rez[i] = prod % 10;
		carry = prod / 10;
	}

	while (carry)
	{
		rez[velikost] = carry % 10;
		carry = carry / 10;
		velikost++;
	}
	return velikost;
}


void factorial(int n)
{
	int rez[MAX];
	rez[0] = 1;
	int velikost = 1;

	for (int i = 2; i <= n; i++) {
		velikost = mnozenje(i, rez, velikost);
	}
	cout << "Faktorijela je \n";
	for (int i = velikost - 1; i >= 0; i--) {
		cout << rez[i];
	}
}




int main() {
	factorial(100);

	
	system("pause");
	return 0;
}